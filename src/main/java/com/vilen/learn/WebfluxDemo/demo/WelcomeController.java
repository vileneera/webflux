package com.vilen.learn.WebfluxDemo.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vilen on 2017/9/29.
 */
@RestController
public class WelcomeController {
    @GetMapping("/")
    public String welcome() {
        return "Hello World";
    }
}
